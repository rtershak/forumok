require 'rails_helper'

RSpec.describe Category, type: :model do
  it 'has a valid factory' do
    create(:category).should be_valid
  end
  it 'is invalid without a title' do
    build(:category, title: nil).should_not be_valid
  end
  it 'is invalid without a body' do
    build(:category, body: nil).should_not be_valid
  end
end
